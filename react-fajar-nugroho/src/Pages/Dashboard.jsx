import { useEffect } from "react"
import { useNavigate } from "react-router-dom"
import api from "../Api"

export default function Dashboard(){

    const navigate = useNavigate()

    const fetchLogout = async () => {

        await api.post('/api/logout').then(() => {
            localStorage.removeItem('token')
            localStorage.removeItem('role_id')

            navigate('/login')
        })
    }

    useEffect(() => {
        if(!localStorage.getItem('token')){
            navigate('/login')
        }
    })

    return (
        <>
            <div className="card">
                <div className="card-body">
                    <h1 className="text-bold">Dashboard</h1>
                    <button onClick={fetchLogout} className="btn btn-danger">Logout</button>
                </div>
            </div>
        </>
    )
}