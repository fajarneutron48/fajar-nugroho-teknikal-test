import { useEffect, useState } from "react"
import api from "../Api"
import { useNavigate } from "react-router-dom"

export default function Login(){
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

    const [errors, setErrors] = useState("")

    const navigate = useNavigate()

    const fetchLogin = async (e) => {
        e.preventDefault()
        const formData = new FormData

        formData.append("email", email)
        formData.append("password", password)

        await api.post('/api/login', formData)
        .then(response => {
            localStorage.setItem('token', response.data.token)
            localStorage.setItem('role_id', response.data.user.role_id)

            navigate('/')
        }).catch(err => setErrors(err.response.data))
    }

    useEffect(() => {
        if(localStorage.getItem('token')){
            navigate('/')
        }
    })

    return (
        <>
            <div className="mt-4">
                <div className="card">
                    <div className="card-body">
                        {
                            errors?.message && (
                                <div className="alert alert-danger">
                                    { errors?.message }
                                </div>
                            )
                        }
                        <form onSubmit={fetchLogin}>
                            <div className="mb-3">
                                <label className="form-label">Email</label>
                                <input type="email" className="form-control" onChange={(e) => setEmail(e.target.value)} />
                                {
                                    errors.email && (
                                        <div className="alert alert-danger mt-2">
                                            { errors.email[0] }
                                        </div>
                                    )
                                }
                            </div>
                            <div className="mb-3">
                                <label className="form-label">Password</label>
                                <input type="password" className="form-control" onChange={(e) => setPassword(e.target.value)} />
                                {
                                    errors.password && (
                                        <div className="alert alert-danger mt-2">
                                            { errors.password[0] }
                                        </div>
                                    )
                                }
                            </div>
                            <button type="submit" className="btn btn-primary">Masuk</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}