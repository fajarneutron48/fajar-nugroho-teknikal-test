import { useState } from "react"
import api from "../Api"
import { useNavigate } from "react-router-dom"

export default function Register(){
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [role_id, setRoleId] = useState("")
    const [password, setPassword] = useState("")
    const [password_confirmation, setPasswordConfirmation] = useState("")

    const [errors, setError] = useState([])

    const navigate = useNavigate()

    const fetchDataRegister = async (e) => {
        e.preventDefault()
        const formData = new FormData

        formData.append('name', name)
        formData.append('email', email)
        formData.append('role_id', role_id)
        formData.append('password', password)
        formData.append('password_confirmation', password_confirmation)

        await api.post('/api/register', formData).then(() => {
            navigate('/login')
        }).catch(err => setError(err.response.data))
    }

    return (
        <>
            <div className="mt-4">
                <div className="card">
                    <div className="card-body">
                        <form onSubmit={fetchDataRegister}>
                            <div className="mb-3">
                                <label className="form-label">Nama</label>
                                <input type="text" className="form-control" onChange={(e) => setName(e.target.value)} />
                                { 
                                    errors.name && (
                                        <div className="alert alert-danger mt-2">
                                            { errors.name[0] }
                                        </div>
                                        )
                                }
                            </div>
                            <div className="mb-3">
                                <label className="form-label">Email</label>
                                <input type="email" className="form-control" onChange={(e) => setEmail(e.target.value)} />
                                { 
                                    errors.email && (
                                        <div className="alert alert-danger mt-2">
                                            { errors.email[0] }
                                        </div>
                                        )
                                }
                            </div>
                            <div className="mb-3">
                                <label className="form-label">Role</label>
                                <select className="form-control" onChange={(e) => setRoleId(e.target.value)}>
                                    <option value="">Pilih</option>
                                    <option value="1">Superadmin</option>
                                    <option value="2">Admin</option>
                                    <option value="3">Member</option>
                                </select>
                                { 
                                    errors.role_id && (
                                        <div className="alert alert-danger mt-2">
                                            { errors.role_id[0] }
                                        </div>
                                        )
                                }
                            </div>
                            <div className="mb-3">
                                <label className="form-label">Password</label>
                                <input type="password" className="form-control" onChange={(e) => setPassword(e.target.value)} />
                            </div>
                            <div className="mb-3">
                                <label className="form-label">Konfirmasi Password</label>
                                <input type="password" className="form-control" onChange={(e) => setPasswordConfirmation(e.target.value)} />
                                { 
                                    errors.password && (
                                        <div className="alert alert-danger mt-2">
                                            { errors.password[0] }
                                        </div>
                                        )
                                }
                            </div>
                            <button type="submit" className="btn btn-primary">Register</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}