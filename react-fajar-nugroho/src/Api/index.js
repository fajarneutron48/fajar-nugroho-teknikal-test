import axios from "axios";

const token = localStorage.getItem("token");

const Api = axios.create({
  baseURL: "https://laravel-api-10.cerise.id",
  headers: { Authorization: `Bearer ${token}` },
});

export default Api;
